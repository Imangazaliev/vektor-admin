require('dotenv').config();

const MODEL_FOLDERS_DEPTH = 3

function createPath(rootPath: string, fileExtension: string): string[] {
    const paths = new Array(MODEL_FOLDERS_DEPTH).fill("")
    return paths.map((_, index) => {
        const depthPath: string[] = new Array(index + 1).fill("/*")
        return rootPath + depthPath.join("") + "." + fileExtension
    })
}

const configs = {
    development: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: "localhost",
        type: "postgres",
        entities: createPath("server/data/models", "ts")
    },
    test: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: "127.0.0.1",
        type: "postgres",
    },
    production: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        type: "postgres",
        entities: createPath("build/data/models", "js")
    }
};

export function getDatabaseConfig() {
    const env = process.env.NODE_ENV || 'development';
    return configs[env];
}

export default {
    ...getDatabaseConfig()
}
