import { Raw } from "typeorm";
import { FindOperator } from "typeorm/find-options/FindOperator";

export enum PgArrayType {
    Undefined = "",
    Int = "::integer[]",
    String = "::varchar[]"
}

export function columnNotIn<T>(
    values: Array<any>,
    type: PgArrayType = PgArrayType.Undefined
): FindOperator<any> {
    return Raw(alias => `${alias} <> ALL (ARRAY[${values.join(', ')}]${type})`)
}
