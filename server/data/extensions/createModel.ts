/**
 * Creates a new entity instance.
 */
export function createModel<T>(modelType: { new(): T }, fields: { [P in keyof T]?: any }): T {
    const model = new modelType();
    const entries = Object.entries(fields);
    for (const [k, v] of entries) {
        model[k] = v;
    }
    return model;
}
