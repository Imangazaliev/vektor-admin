import { createQueryBuilder, EntityManager, getManager, QueryBuilder } from 'typeorm'
import *as _ from 'lodash'
import { snakeCase } from "snake-case"

declare type UpsertOptions<T, M = keyof T> = {
    table: { new(): T },
    values: T[],
    doNotUpsert?: Array<M>,
    uniqueFields?: Array<M>,
    entityManager?: EntityManager
}

export async function upsert<T>(options: UpsertOptions<T>) {
    if (!options.values.length) {
        return;
    }

    const doNotUpsertKeyNames = options.doNotUpsert ? options.doNotUpsert.map(key => key as string) : [];
    const keys: string[] = _.difference(_.keys(options.values[0]), doNotUpsertKeyNames);
    const setterString = keys.map(k => `"${snakeCase(k)}" = excluded.${snakeCase(k)}`).join(", ");

    const manager = (options.entityManager || getManager());
    for (const model of options.values) {
        const uniqueFieldsClause = options.uniqueFields?.map(field => ({ [field]: model[field] })) || [];
        const whereClause = [{ id: model["id"] }, ...uniqueFieldsClause];

        const exists = (await manager.createQueryBuilder(options.table, options.table.name)
            .select()
            .where(whereClause)
            .getCount()) > 0;

        if (exists) {
            await manager.createQueryBuilder()
                .update(options.table)
                .set(model)
                .where(whereClause)
                .execute();
        } else {
            await manager
                .createQueryBuilder()
                .insert()
                .into(options.table)
                .values(model)
                .onConflict(`(id) DO UPDATE SET ${setterString}`)
                .execute();
        }
    }
}
