import { getRepository } from "typeorm";
import { FindManyOptions } from "typeorm/find-options/FindManyOptions";

export interface PaginateResult<TAttributes> {
    items: Array<TAttributes>
    pages: number
    total: number
}

export interface PaginateOptions<M> extends FindManyOptions<M> {
    page: number, // Default 1
    pageSize: number, // Default 25
}

export async function findWithPagination<M>(
    table: { new(): M },
    options: PaginateOptions<M>
): Promise<PaginateResult<any>> {
    const page = options.page ? options.page : 1;
    const pageSize = options.pageSize ? options.pageSize : 25;

    const repository = getRepository<M>(table);
    let total = await repository.count(options);

    const pages = Math.ceil(total / pageSize);
    const items = await repository.find({ ...options, skip: pageSize * (page - 1), take: pageSize });
    return { items, pages, total }
}
