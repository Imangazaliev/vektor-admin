import "reflect-metadata";
import * as express from "express";
import { Application } from "express";
import { Container } from "typedi";
import * as path from "path";
import { AppRoot, FilesFolder, ServerUrl } from "./di/diTokens";
import { getLocalIp } from "./extensions/miscExtensions";
import { AccessControl } from "./presentation/rbac/accessControl"
import { ServerConfigurator } from './presentation/express/configurator';

require('./extensions');
require('dotenv').config();

export class KalimatApplication {

    private expressApp: Application;
    private accessControl = new AccessControl()

    constructor(private config: ApplicationConfig) {
        Container.set(AccessControl, this.accessControl);
        Container.set(AppRoot, __dirname);
        Container.set(FilesFolder, path.resolve(__dirname, "files", ""));
    }

    async run() {
        await this.accessControl.initialize();

        this.expressApp = express();
        const configurator = Container.get(ServerConfigurator);
        configurator.configServer(this.expressApp);

        const isLocal = process.env.IS_LOCAL //set IS_LOCAL=true && "server running command"
        const host = isLocal ? getLocalIp() : 'localhost'
        const serverUrl = isLocal ? `http://${host}:${this.config.port}` : process.env.WEBSITE_URL
        Container.set(ServerUrl, serverUrl);

        //Start our app, listen for requests
        const server = this.expressApp.listen(this.config.port, 'localhost', () => {
            console.log(`Server running at http://localhost:${this.config.port}`);
        });
    }

}

export class ApplicationConfig {

    constructor(public port: number) {

    }

}
