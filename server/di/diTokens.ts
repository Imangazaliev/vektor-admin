import { Token } from "typedi";
import winston = require("winston");

export const AppRoot = new Token<string>();

export const FilesFolder = new Token<string>();

export const ServerUrl = new Token<string>();

export const Logger = new Token<winston.Logger>();
