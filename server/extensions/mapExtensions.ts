export {}

declare global {

    interface Map<K, V> {

        putInArray<K, V>(this: Map<K, V[]>, key: K, value: V): boolean;

        map<Key, Value, Result>(this: Map<K, V>, mapper: (value: Value) => Result): Map<Key, Result>;

        mapAsync<Key, Value, Result>(this: Map<K, V>, mapper: (value: Value) => Promise<Result>): Promise<Map<Key, Result>>;

    }

}

Map.prototype.putInArray = function <K, V>(this: Map<K, V[]>, key: K, value: V): boolean {
    let isNew: boolean = false;
    if (!this.has(key)) {
        this.set(key, []);
        isNew = true;
    }
    this.get(key)!.push(value);
    return isNew;
};


Map.prototype.map = function <Key, Value, Result>(this: Map<Key, Value>, mapper: (value: Value) => Result): Map<Key, Result> {
    const newMap = new Map<Key, Result>();
    this.forEach((value, key) => {
        newMap.set(key, mapper(value));
    });
    return newMap;
};

Map.prototype.mapAsync = async function <Key, Value, Result>(this: Map<Key, Value>, mapper: (value: Value) => Promise<Result>): Promise<Map<Key, Result>> {
    return new Promise(async (resolve, reject) => {
        const newMap = new Map<Key, Result>();
        for (const [key, value] of this) {
            await newMap.set(key, await mapper(value));
        }
        resolve(newMap)
    });
};
