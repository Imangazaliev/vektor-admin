export function isDevMode(): boolean {
    return process.env.NODE_ENV === 'development'
}


export function getLocalIp() {
    const os = require('os');
    const networkInterfaces = os.networkInterfaces();

    const ipAdresses = []
    Object.keys(networkInterfaces).forEach(ifname => {
        let alias = 0;
        networkInterfaces[ifname].forEach((iface, ifname) => {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }

            if (alias >= 1) {
                // this single interface has multiple ipv4 addresses
                //console.log(ifname + ':' + alias, iface.address);
                ipAdresses.push(iface.address)
            } else {
                // this interface has only one ipv4 adress
                //console.log(ifname, iface.address);
                ipAdresses.push(iface.address)
            }
            ++alias;
        });
    });

    return ipAdresses[1]
}
