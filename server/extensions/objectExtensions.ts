export function exclude<T, R>(object: T, ...excludedFields: Array<keyof T>): R {
    const newObj: any = { ...object };
    excludedFields.forEach(field => {
        delete newObj[field]
    });
    return newObj as R
}
