import { Container } from 'typedi';
import "reflect-metadata";

export function Autowired(name?: string) {
    return function(target: Object, propertyName: string){
        let type = Reflect.getMetadata('design:type', target, propertyName);
        target[propertyName] = Container.get(name || type)
    }
}
