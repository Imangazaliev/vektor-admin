import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";

type AppRoute = (req: Request, res: Response, next?: NextFunction) => any

export function wrapRoute(
    route: AppRoute
): AppRoute {
    return async(req: Request, res: Response, next?: NextFunction) => {
        try {
            await route(req, res, next)
        } catch (error) {
            next(error)
        }
    }
}
