export {}

declare global {

    interface Array<T> {
        toMap<Key, Value>(key: keyof Value): Map<Key, Value>

        groupBy<Key, Value>(this: Value[], keyField: keyof Value): Map<Key, Value[]>;

        groupAndMap<Key, Value, Result>(this: Value[], keyField: keyof Value, converter?: (item: Value) => Result): Map<Key, Result[]>;

    }

}

Array.prototype.toMap = function <Key, Value> (this: Value[], key: keyof Value) : Map<Key, Value> {
    const result = this.map(item => [item[key], item]);
    //@ts-ignore
    return new Map<Key, Value>(result);
};

Array.prototype.groupBy = function <Key, Value>(this: Value[], keyField: keyof Value): Map<Key, Value[]> {
    const map = new Map<Key, Value[]>();
    this.forEach(item => {
        //@ts-ignore
        map.putInArray(item[keyField], item);
    });
    return map;
};

Array.prototype.groupAndMap = function <Key, Value, Result>(this: Value[], keyField: keyof Value, mapper: (item: Value) => Result): Map<Key, Result[]> {
    const map = new Map<Key, Result[]>();
    this.forEach(item => {
        //@ts-ignore
        map.putInArray(item[keyField], mapper(item));
    });
    return map;
};
