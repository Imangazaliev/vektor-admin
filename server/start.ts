import { ApplicationConfig, KalimatApplication } from "./kalimatApplication";

require('dotenv').config();

const config = new ApplicationConfig(+process.env.SERVER_PORT);
const app = new KalimatApplication(config);

(async() => {
    await app.run();
})();
