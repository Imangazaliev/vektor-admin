import { NextFunction, Request, Response } from "express";
import profile from "./myProfile"

export async function login(req: Request, res: Response, next: NextFunction) {
    res.json(profile)
}

export async function logout(req: Request, res: Response) {
    res.status(200).send()
}
