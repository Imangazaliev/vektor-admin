export default {
  name: "Murad",
  role: "student",
  email: "murad@gmail.com",
  userName: "Murad",
  permissions: [
    "schools:view",
    "clubs:view",
    "courses:view",
    "universities:view",
    "companies:view",
    "rating:view",
  ]
}