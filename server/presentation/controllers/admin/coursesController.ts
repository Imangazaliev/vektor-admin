import { Request, Response } from "express";

export class CoursesController {

    getCoursesList(req: Request, res: Response) {
        res.status(200).json({
            pageCount: 1,
            itemCount: 5,
            courses: [
                {
                    name: "Профессия Android-разработчик",
                    description: "Вы освоите разработку под самую популярную мобильную платформу, создадите своё приложение и выложите его в Google Play, даже если до этого вы никогда не программировали.",
                    tags: ["Java", "Kotlin"],
                    status: "planned",
                    clubName: "Школа Samsung",
                    speciality: "Android-разработчик",
                    startDate: "01.06.2021",
                    endDate: "30.07.2021",
                    price: 6555,
                    minGrade: 7,
                    maxGrade: 9,
                    imageUrl: 69,
                    lessonCount: 69,
                    lessonsCompleted: 69,
                    participants: []
                },
                {
                    name: "Введение в программирование: Snap!",
                    description: "Продвинутый вариант Scratch, позволяющий создавать свои блоки на языке Javascript",
                    tags: [],
                    status: "active",
                    clubName: "IT-Cube",
                    speciality: null,
                    startDate: "20.02.2021",
                    endDate: "20.05.2021",
                    price: 482,
                    minGrade: 5,
                    maxGrade: 9,
                    imageUrl: "https://media.grodno.in/source/photos/2018/04/03/28873403.png",
                    lessonCount: 20,
                    lessonsCompleted: 12,
                    participants: [4, 6, 2, 7]
                },
                {
                    name:  "Алгоритмы и структуры данных",
                    description: "Вы расширите кругозор и получите опыт реализации классических алгоритмов, который поможет вам при создании собственных алгоритмов для решения бизнес-задач.",
                    tags: ["Java", "Python"],
                    status: "finished",
                    clubName: "Яндекс школа",
                    speciality: "Анализ данных",
                    startDate: "01.10.2020",
                    endDate: "25.12.2020",
                    price: 0,
                    minGrade: 9,
                    maxGrade: 11,
                    imageUrl: "https://go4course.com/Content/app_images/Courses/69e0156e-f182-4a30-aedd-5762f9dd618b.png",
                    lessonCount: 69,
                    lessonsCompleted: 69,
                    participants: [1, 10, 8, 3, 4]
                }
            ]
        })
    }

    getCourseInfo() {

    }

}

export default new CoursesController();
