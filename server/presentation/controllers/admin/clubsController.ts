import { Request, Response } from "express";

export class ClubsController {

    getClubsList(req: Request, res: Response) {
        res.status(200).json({
            pageCount: 1,
            itemCount: 5,
            clubs: [
                {
                    id: 1,
                    name: "IT-Cube",
                    city: "Махачкала",
                    address: "Ш. Алиева 87Б",
                    rating: 4.9
                },
                {
                    id: 2,
                    name: "Школа Samsung",
                    city: "Махачкала",
                    address: "Шамиля 5",
                    rating: 4.5
                },
                {
                    id: 3,
                    name: "Яндекс Школа",
                    city: "Махачкала",
                    address: "Ленина 7А",
                    rating: 4.6
                },
                {
                    id: 4,
                    name: "Лаборатория программирования",
                    city: "Махачкала",
                    address: "Гамидова, 18Ж",
                    rating: 4.3
                },
                {
                    id: 5,
                    name: "Софтиум",
                    city: "Махачкала",
                    address: "Гагарина, 72А, каб. 2",
                    rating: 4.7
                },
            ]
        })
    }

    getClubInfo() {

    }

}

export default new ClubsController();
