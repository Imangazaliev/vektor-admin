import { Request, Response } from "express";
import { Container } from "typedi";
import { AccessControl } from "../../../rbac/accessControl";
import { PermissionCode } from "../../../rbac/permissionCode";

export default async function(request: Request, response: Response) {
    const {
        code,
        permissions
    }: {
        code: string,
        title: string,
        description: string,
        permissions: PermissionCode[]
    } = request.body;
    Container.get(AccessControl).updateRolePermissions(code, permissions);
    response.status(200).send()
}
