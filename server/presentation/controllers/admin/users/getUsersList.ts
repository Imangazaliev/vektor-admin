import { Request, Response } from 'express'
import { PaginateOptions } from "../../../../data/extensions/pagination";
import { FindConditions } from "typeorm/find-options/FindConditions";
import { Raw } from "typeorm";

export default async function(req: Request, res: Response) {
    const page = req.query.page;
    const pageSize = req.query.pageSize;
    const userName = req.query.userName;
    const orderBy = req.query.orderBy;
    const orderDesc = req.query.orderDesc;

    res.status(200).json({ users: [], pageCount: 10, userCount: 100 })
}
