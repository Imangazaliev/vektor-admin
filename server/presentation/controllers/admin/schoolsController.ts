import { Request, Response } from "express";

export class SchoolsController {

    getSchoolsList(req: Request, res: Response) {
        res.status(200).json({
            schools: [
                { id: 1, name: "РМЛ", city: "Махачкала", rating: 4.4 },
                { id: 2, name: "Лицей 8", city: "Махачкала", rating: 4.1 },
                { id: 3, name: "Лицей 39", city: "Махачкала", rating: 3.6 },
                { id: 4, name: "Гимназия 38", city: "Махачкала", rating: 3.8 },
                { id: 5, name: "Школа №48", city: "Новый Кяхулай", rating: 3.8 },
                { id: 6, name: "Махачкалинский лицей №22", city: "Махачкала", rating: 4 },
                { id: 7, name: "Лицей 5", city: "Махачкала", rating: 4.3 },
                { id: 8, name: "Школа 4", city: "Махачкала", rating: 4.4 },
                { id: 9, name: "Школа 16", city: "Махачкала", rating: 4.2 },
                { id: 10, name: "Школа №32", city: "Махачкала", rating: 3.2 },
                { id: 11, name: "Лицей №51", city: "Махачкала", rating: 3.2 },
                { id: 12, name: "Школа №43", city: "Богатыревка", rating: 3.8 },
                { id: 13, name: "Лицей 52", city: "Махачкала", rating: 4 },
                { id: 14, name: "Школа №18", city: "Махачкала", rating: 3.1 },
                { id: 15, name: "Школа 56", city: "Махачкала", rating: 3.5 }
            ]
        })
    }

    getSchoolsInfo() {

    }

}

export default new SchoolsController();
