import { RBACRole } from "./rbacRole";
import { PermissionCode } from "./permissionCode";
import { createModel } from "../../data/extensions/createModel";

export class AccessControl {

    private roles: Map<string, RBACRole>;

    async initialize() {
        this.roles = new Map();
        this.roles.set("admin", createModel(RBACRole, {
            roleCode: "",
            permissions: ['admin-panel:view']
        }))
    }

    hasPermission(role: string, permission: PermissionCode): boolean {
        return this.roles.get(role)!.hasPermission(permission)
    }

    updateRolePermissions(roleCode: string, permissions: PermissionCode[]) {
        this.roles.get(roleCode).updatePermissions(permissions)
    }

}
