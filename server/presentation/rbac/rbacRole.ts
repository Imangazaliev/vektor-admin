import { PermissionCode } from "./permissionCode";

export class RBACRole {

    public roleCode!: string
    public permissions!: PermissionCode[]

    hasPermission(permissionCode: PermissionCode): boolean {
        return this.permissions.includes(permissionCode)
    }

    updatePermissions(permissions: PermissionCode[]) {
        this.permissions = permissions
    }

}
