import { NextFunction, Request, RequestHandler, Response } from "express";
import { AccessControl } from "./accessControl";
import { PermissionCode } from "./permissionCode";
import { Inject } from "typedi";
import { UserRM } from "../models/admin/userRM";

export class AccessControlMiddleware {

    @Inject()
    accessControl: AccessControl;

    checkAuthorized(): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            if (req?.user) {
                next()
            } else {
                res.status(401).send({
                    message: 'User is not authorized',
                    error_code: 'NOT_AUTHORIZED'
                });
            }
        }
    }

    checkPermission(permission: PermissionCode): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            if (req.isAuthenticated()) {
                if (this.hasPermission(<UserRM> req.user, permission)) {
                    next()
                } else {
                    res.status(403).json({
                        message: "Forbidden access",
                        error_code: "FORBIDDEN_ACCESS"
                    })
                }
            } else {
                res.status(401).json({
                    message: "User is not authenticated",
                    error_code: "NOT_AUTHENTICATED"
                })
            }
        }
    }

    private hasPermission(user: UserRM, permission: PermissionCode) : boolean {
        return this.accessControl.hasPermission(user.role, permission)
    }

}
