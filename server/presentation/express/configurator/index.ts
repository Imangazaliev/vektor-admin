import * as express from 'express'
import { Application } from 'express'
import { configBodyParser } from './bodyParserConfig'
import { MainRoutesConfigurator } from "../../routes";
import { configStatic } from "./staticConfig";
import { LoggingConfigurator } from "./loggingConfigurator";
import { Container, Inject } from "typedi";
import { ErrorHandlerConfigurator } from "./errors/errorHandlerConfigurator";
import { CorsConfigurator } from "./corsConfigurator";
import fileUpload = require("express-fileupload");
import { FilesFolder } from "../../../di/diTokens";
import * as path from "path";

export class ServerConfigurator {

    @Inject()
    loggingConfigurator: LoggingConfigurator;
    @Inject()
    mainRoutesConfigurator: MainRoutesConfigurator;
    @Inject()
    errorHandlerConfigurator: ErrorHandlerConfigurator;
    @Inject()
    corsConfigurator: CorsConfigurator;

    configServer(app: Application) {
        const filesFolder = Container.get(FilesFolder)
        app.use(fileUpload({
            useTempFiles: true,
            limits: { fileSize: 10 * 1024 * 1024, },
            tempFileDir: path.join(filesFolder, "temp")
        }));

        configBodyParser(app);
        this.corsConfigurator.config(app);

        this.loggingConfigurator.configLogging(app);

        app.use('/files', express.static(filesFolder));
        const apiRouter = this.mainRoutesConfigurator.configRoutes();
        app.use('/api', apiRouter);
        configStatic(app);

        this.loggingConfigurator.configErrorLogging(app);
        this.errorHandlerConfigurator.configure(app)
    }

}
