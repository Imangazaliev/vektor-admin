import { Application } from 'express';
import * as cors from 'cors';
import { isDevMode } from "../../../extensions/miscExtensions";
import { Inject } from "typedi";
import { Logger } from "../../../di/diTokens";
import * as winston from "winston";

export class CorsConfigurator {

    @Inject(Logger)
    private logger: winston.Logger;

    config(app: Application) {
        const allowedOrigins = isDevMode() ?
            ['http://localhost:8080', 'http://localhost:3000'] //dev
            : ['https://kalimat.su']; //production
        const corsConfig: cors.CorsOptions = {
            credentials: true,
            origin: (requestOrigin, callback) => {
                if (!requestOrigin) return callback(null, true);
                if (allowedOrigins.indexOf(requestOrigin) === -1) {
                    const msg = 'The CORS policy for this site does not ' +
                        'allow access from the specified origin: ' + requestOrigin;
                    return callback(new Error(msg), false)
                }
                return callback(null, true)
            }
        };

        app.use(cors(corsConfig));
        app.options('*', cors(corsConfig))
    }

}
