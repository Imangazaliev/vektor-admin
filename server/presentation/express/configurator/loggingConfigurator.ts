import { Application } from "express"
import { isDevMode } from "../../../extensions/miscExtensions";
import * as winston from 'winston';
import * as  expressWinston from 'express-winston';
import { Container } from "typedi";
import * as path from "path";
import { AppRoot, Logger } from "../../../di/diTokens";

export class LoggingConfigurator {

    loggingTransports: any[];
    exceptionsHandlers: any[];

    constructor() {
        const rootFolder: string = Container.get(AppRoot);
        const logsFolder = path.join(rootFolder, '..', 'server-logs');
        const errorLogFile = path.join(logsFolder, 'errors.log');
        const fullLogFile = path.join(logsFolder, 'full.log');

        this.loggingTransports = [
            new winston.transports.File({ filename: errorLogFile, level: 'error' }),
            new winston.transports.File({ filename: fullLogFile })
        ];
        this.exceptionsHandlers = [
            new winston.transports.File({ filename: errorLogFile }),
            new winston.transports.File({ filename: fullLogFile })
        ];

        if (isDevMode()) {
            //this.loggingTransports.push(new winston.transports.Console({
            //    level: 'debug',
            //    handleExceptions: true,
            //    format: winston.format.combine(
            //        winston.format.timestamp(),
            //        winston.format.json(),
            //        winston.format.prettyPrint()
            //    )
            //}));
        }

        const logger = winston.createLogger({
            transports: this.loggingTransports,
            exceptionHandlers: this.exceptionsHandlers
        });
        Container.set(Logger, logger)
    }

    //should be before router
    configLogging(app
                      :
                      Application
    ) {
        app.use(expressWinston.logger({
            transports: this.loggingTransports,
            format: winston.format.combine(
                winston.format.simple()
                //winston.format.json()
            ),
            msg: "HTTP {{req.method}} {{req.url}}",
            expressFormat: true,
            colorize: false,
            ignoreRoute: function(req, res) {
                return false;
            } // optional: allows to skip some log messages based on request and/or response
        }));
    }

    //should be after router
    configErrorLogging(app
                           :
                           Application
    ) {
        app.use(expressWinston.errorLogger({
            transports: this.loggingTransports,
            format: winston.format.combine(
                winston.format.json()
            )
        }));
    }


}


