import { Application } from 'express';
import * as bodyParserConfig from 'body-parser';

const boolParser = require('express-query-boolean');

export function configBodyParser(app: Application) {
    app.use(bodyParserConfig.urlencoded({ extended: true }));
    app.use(bodyParserConfig.json());
    app.use(boolParser());
}
