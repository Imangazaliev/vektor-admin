import * as express from "express"
import { Application } from "express"
import { Container } from "typedi";
import * as path from "path";
import { isDevMode } from "../../../extensions/miscExtensions";
import { AppRoot } from "../../../di/diTokens";

//must be after routes
export function configStatic(app: Application) {
    if (!isDevMode()) {
        const appRootDir: string = Container.get(AppRoot);
        // Static folder
        app.use(express.static(path.join(appRootDir, '/public/')));
        // handle every other route with index.html, which will contain
        // a script tag to your application's JavaScript file(s).
        app.get('*', function(req, res) {
            res.sendFile(path.join(appRootDir, 'public', 'index.html'));
        });
    }
}
