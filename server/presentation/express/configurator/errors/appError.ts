export class AppError extends Error {

    public statusCode: number;
    public code: string;
    public description: string;

    constructor(
        { statusCode = 500, code, description = null }: {
            statusCode?: number,
            code: string,
            description?: string
        }
    ) {
        super();

        Object.setPrototypeOf(this, AppError.prototype);

        this.statusCode = statusCode;
        this.code = code;
        this.description = description
    }

    toString() {
        return 'Status code: ' + this.statusCode + '\n. Code: ' + this.code + '\n. Message: ' + this.message;
    }

}
