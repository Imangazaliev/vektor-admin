import { Application, Request, Response } from "express"
import { Inject } from "typedi";
import * as winston from "winston";
import { Logger } from "../../../../di/diTokens";
import { isDevMode } from "../../../../extensions/miscExtensions";
import { AppError } from "./appError";

export class ErrorHandlerConfigurator {

    @Inject(Logger)
    private logger: winston.Logger;

    configure(app: Application) {
        app.use((err, req, res, next) => {
            this.handleError(err, req, res);
        });
    }

    private handleError(err: Error, req: Request, res: Response) {
        let statusCode: number;
        let code: string;
        let description: string;

        if (err instanceof AppError) {
            statusCode = err.statusCode;
            code = err.code;
            description = err.description;
        } else {
            statusCode = 500;
            code = "UNKNOWN_ERROR";
            description = err.message
        }

        if (isDevMode()) {
            console.log("Error Handler: ", err);
        }

        this.logger.error({
            message: JSON.stringify({
                url: req.query,
                statusCode,
                code,
                description
            })
        });

        res.status(statusCode).json({
            code,
            description
        });
    }

}
