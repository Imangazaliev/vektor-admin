import { Strategy as LocalStrategy } from 'passport-local'
import * as bcrypt from 'bcryptjs';
import { usersRepository } from '../../../data/repositories';
import { UserRM } from "../../models/admin/userRM";

function isValidPassword(user: any, password: string): boolean {
    return bcrypt.compareSync(password, user.passwordHash)
}

export default new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: false
    },
    async(userEmail: string, password: string, done) => {
        try {
            const user: UserRM = await usersRepository.getUserByEmail(userEmail);
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' })
            }
            if (password == undefined || !isValidPassword(user, password)) {
                return done(null, false, { message: 'Incorrect password.' })
            }

            done(null, user)
        } catch (error) {
            console.log(error);
            done(error)
        }
    }
)
