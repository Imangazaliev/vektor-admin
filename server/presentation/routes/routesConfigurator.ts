import { Router } from "express";
import { AccessControlMiddleware } from "../rbac/accessControlMiddleware";

export abstract class RoutesConfigurator {

    constructor(public parentPath = '') {
    }

   abstract configRoutes(middleware: AccessControlMiddleware) : Router

}
