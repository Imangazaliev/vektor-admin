import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import studentsController from '../../controllers/admin/studentsController'
import { wrapRoute } from "../../../extensions/expressExtensions";

export class StudentsRouter extends RoutesConfigurator {

    constructor() {
        super('/ztudents')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .post('/', wrapRoute(studentsController.getStudentsRating))
        return router
    }

}
