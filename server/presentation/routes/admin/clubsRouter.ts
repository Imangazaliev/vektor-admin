import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import clubsRouter from '../../controllers/admin/clubsController'
import { wrapRoute } from "../../../extensions/expressExtensions";

export class ClubsRouter extends RoutesConfigurator {

    constructor() {
        super('/clubs')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .post('/', wrapRoute(clubsRouter.getClubsList))
            .post('/:id', wrapRoute(clubsRouter.getClubInfo))
        return router
    }

}
