import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import coursesController from '../../controllers/admin/coursesController'
import { wrapRoute } from "../../../extensions/expressExtensions";

export class CoursesRouter extends RoutesConfigurator {

    constructor() {
        super('/courses')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .post('/', wrapRoute(coursesController.getCoursesList))
            .post('/:id', wrapRoute(coursesController.getCourseInfo))
        return router
    }

}
