import { Router } from 'express'
import { RoutesConfigurator } from "../../routesConfigurator";
import { AccessControlMiddleware } from "../../../rbac/accessControlMiddleware";

export class SettingsRouter extends RoutesConfigurator {

    constructor() {
        super('/settings')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        return router
    }

}
