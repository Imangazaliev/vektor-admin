import { Router } from 'express'
import { RoutesConfigurator } from "../../routesConfigurator";
import { AccessControlMiddleware } from "../../../rbac/accessControlMiddleware";
import * as usersController from '../../../controllers/admin/users'
import { wrapRoute } from "../../../../extensions/expressExtensions";

export class UsersRouter extends RoutesConfigurator {

    constructor() {
        super('/users')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .get('/', middleware.checkPermission('users:manage'),
                wrapRoute(usersController.getUsersList))
            .get('/:id', middleware.checkPermission('users:manage'),
                wrapRoute(usersController.getUserById))
            .post('/:id', middleware.checkPermission('users:manage'),
                wrapRoute(usersController.saveUser));
        return router
    }

}
