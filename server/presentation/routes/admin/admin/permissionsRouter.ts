import { Router } from 'express'
import { RoutesConfigurator } from "../../routesConfigurator";
import { AccessControlMiddleware } from "../../../rbac/accessControlMiddleware";
import * as permissionsController from '../../../controllers/admin/permissions'
import { wrapRoute } from "../../../../extensions/expressExtensions";

export class PermissionsRouter extends RoutesConfigurator {

    constructor() {
        super('/permissions')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .get('/', wrapRoute(permissionsController.getPermissionsList))
            .get('/:id',  wrapRoute(permissionsController.getPermissionById))
            .post('/:id', wrapRoute(permissionsController.updatePermission));
        return router
    }

}
