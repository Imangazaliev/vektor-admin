import { Router } from 'express'
import { RoutesConfigurator } from "../../routesConfigurator";
import { AccessControlMiddleware } from "../../../rbac/accessControlMiddleware";
import * as rolesController from '../../../controllers/admin/roles'
import { wrapRoute } from "../../../../extensions/expressExtensions";

export class RolesRouter extends RoutesConfigurator {

    constructor() {
        super('/roles')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .get('/', middleware.checkPermission('users:manage'),
                wrapRoute(rolesController.getRolesList))
            .get('/:id', middleware.checkPermission('users:manage'),
                wrapRoute(rolesController.getRoleById))
            .post('/:id', middleware.checkPermission('users:manage'),
                wrapRoute(rolesController.saveRole));
        return router
    }

}
