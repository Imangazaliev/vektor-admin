import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import schoolsController from '../../controllers/admin/schoolsController'
import { wrapRoute } from "../../../extensions/expressExtensions";

export class SchoolsRouter extends RoutesConfigurator {

    constructor() {
        super('/schools')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .post('/', wrapRoute(schoolsController.getSchoolsList))
            .post('/:id', wrapRoute(schoolsController.getSchoolsInfo))
        return router
    }

}
