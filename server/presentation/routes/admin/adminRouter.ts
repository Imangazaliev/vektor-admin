import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import { AuthRoutes } from "../user/authRouter";
import { ProfileRoutes } from "../user/profileRouter";
import { UsersRouter } from "./admin/usersRouter";
import { PermissionsRouter } from "./admin/permissionsRouter";
import { RolesRouter } from "./admin/rolesRouter";
import { SchoolsRouter } from "./schoolsRouter";
import { configRoutes } from "../index";
import { SettingsRouter } from "./admin/settingsRouter";
import { ClubsController } from "../../controllers/admin/clubsController";
import { ClubsRouter } from "./clubsRouter";
import { StudentsRouter } from "./studentsRouter";
import { CoursesRouter } from "./coursesRouter";

export class AdminRouter extends RoutesConfigurator {

    constructor() {
        super('/admin')
    }

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();

        const routeConfigurators = [
            new AuthRoutes(),
            new ProfileRoutes(),
            new UsersRouter(),
            new PermissionsRouter(),
            new RolesRouter(),
            new UsersRouter(),
            new SettingsRouter(),
            new ClubsRouter(),
            new CoursesRouter(),
            new SchoolsRouter(),
            new StudentsRouter()
        ];

        router.use(configRoutes(routeConfigurators, middleware));
        return router
    }

}
