import {NextFunction, Request, Response, Router} from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import { wrapRoute } from "../../../extensions/expressExtensions";
import profile from "../../controllers/myProfile"


function auth(req: Request, res: Response, next: NextFunction) {
    res.json(profile)
}

export class ProfileRoutes extends RoutesConfigurator {

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .get('/profile', wrapRoute(auth));
        return router
    }

}
