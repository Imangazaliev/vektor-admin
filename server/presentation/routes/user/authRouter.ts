import { Router } from 'express'
import { RoutesConfigurator } from "../routesConfigurator";
import { AccessControlMiddleware } from "../../rbac/accessControlMiddleware";
import { wrapRoute } from "../../../extensions/expressExtensions";
import * as authController from '../../controllers/auth'

export class AuthRoutes extends RoutesConfigurator {

    configRoutes(middleware: AccessControlMiddleware): Router {
        const router = Router();
        router
            .post('/login', wrapRoute(authController.login))
            .get('/logout', middleware.checkAuthorized(), wrapRoute(authController.logout));
        return router
    }

}
