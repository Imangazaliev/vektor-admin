import { Router } from 'express'
import { AccessControlMiddleware } from "../rbac/accessControlMiddleware";
import { RoutesConfigurator } from "./routesConfigurator";
import { Container } from "typedi";
import { AdminRouter } from "./admin/adminRouter";

export class MainRoutesConfigurator {

    configRoutes() : Router {
        const accessControlMiddleware = Container.get(AccessControlMiddleware);

        const routeConfigurators = [
            new AdminRouter()
        ];
        return configRoutes(routeConfigurators, accessControlMiddleware)
    }

}

export function configRoutes(
    routeConfigurators: RoutesConfigurator[],
    accessControlMiddleware: AccessControlMiddleware
) : Router {
    const parentRouter = Router();
    routeConfigurators.forEach(configurator => {
        const router = configurator.configRoutes(accessControlMiddleware);
        parentRouter.use(configurator.parentPath, router)
    });
    return parentRouter
}
