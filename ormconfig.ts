import { getDatabaseConfig } from "server/data/config";

export  = {
   ...getDatabaseConfig(),
   migrations: [
      "server/data/migrations/*.ts"
   ],
   subscribers: [
      "server/data/subscriber/**/*.ts"
   ],
   cli: {
      entitiesDir: "server/data/models",
      migrationsDir: "server/data/migrations",
      subscribersDir: "server/data/subscribers"
   }
};
