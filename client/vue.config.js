const path = require('path')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const { merge } = require('webpack-merge')
const tsImportPluginFactory = require('ts-import-plugin')

module.exports = {
    outputDir: path.resolve(__dirname, '../build/public'),
    transpileDependencies: [
        /\bvue-echarts\b/,
        /\bresize-detector\b/,
        /\bvue-c3\b/,
        /\bvue-masonry\b/,
        /\bvue-cropper\b/,
        /\bvuedraggable\b/
    ],
    lintOnSave: false,
    configureWebpack: {
        entry: path.join(__dirname, 'src', 'main.ts'),
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.json'],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                    options: {
                        appendTsSuffixTo: [/\.vue$/],
                    },
                },
            ],
        },
    },
    chainWebpack: config => {
        config.module.rules.delete('eslint')
        // Add "node_modules" alias
        config.resolve.alias
            .set('node_modules', path.join(__dirname, './node_modules'))

        // Disable "prefetch" plugin since it's not properly working in some browsers
        config.plugins
            .delete('prefetch')

        // Do not remove whitespaces
        config.module.rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                options.compilerOptions.preserveWhitespace = true
                return options
            })
    }
}
