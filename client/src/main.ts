import Vue from 'vue'
import KalimatApp from './App.vue'
import router from './router/index'
import store from './store'
import mixins from "./mixins/shared/mixins";

import BootstrapVue from 'bootstrap-vue'
import GlobalsMixin from './mixins/globalsMixin'
import Popper from 'popper.js'
import Vuelidate from 'vuelidate'
import VModal from 'vue-js-modal'
import Toasted from 'vue-toasted'
import Notifications from 'vue-notification'
import VueGtag from "vue-gtag";

// Required to enable animations on dropdowns/tooltips/popovers
// @ts-ignore
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

Vue.config.productionTip = false;

Vue.directive('click-outside', {
    bind: function (el: any, binding: any, vnode: any) {
        el.clickOutsideEvent = function (event: any) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el: any) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});

Vue.use(require('vue-shortkey'))
Vue.use(VueGtag, {
    config: { id: "UA-167344941-1" }
});
Vue.use(BootstrapVue);
Vue.use(Notifications);
Vue.use(Toasted, {
    theme: 'bubble',
    iconPack: 'fontawesome',
    duration: 3000
});
Vue.use(Vuelidate);
Vue.use(VModal, {
    dynamic: true,
    injectModalsContainer: true
});

Vue.mixin(mixins);
// Global RTL flag
Vue.mixin(GlobalsMixin);


async function startFrontEnd() {
    await store.dispatch('fetchUserData')
    new Vue({
        router,
        store,
        render: h => h(KalimatApp)
    }).$mount('#app')
}

startFrontEnd()
