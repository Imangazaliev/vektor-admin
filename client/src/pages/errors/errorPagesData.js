module.exports = {
    404: {
        code: 404,
        codeDescription: "Страница не найдена",
        fullMessage: "Мы не нашли страницу,<br/>которую вы искали :(",
        showBackButton: true
    },
    500: {
        code: 500,
        codeDescription: "Ошибка сервера",
        fullMessage: "Произошла ошибка<br/> на стороне сервера :(",
        showBackButton: false
    },
}
