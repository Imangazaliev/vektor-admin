import Vue from 'vue'
import VueRouter, { Route } from 'vue-router'
import { IGlobalsMixin } from "./global-mixins";

declare module "vue/types/vue" {
    interface Vue extends IGlobalsMixin {

        //$notify: (options: NotificationOptions | string) => void;

        showSuccessNotification(title: string, message: string): any

        showSuccessToast({ text }: { text: string }): any

        showErrorToast({ text }: { text: string }): any

        showToast(
            { text, type, icon }:
                { text: string, type?: string, icon?: string }
        ): any

        addQueryParam(
            parameter: string,
            value: string | undefined
        ): any

    }
}
