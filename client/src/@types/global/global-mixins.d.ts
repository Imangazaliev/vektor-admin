import HelperFuncs from "../../mixins/helpersTs";

export interface IGlobalsMixin {

    layoutHelpers: HelperFuncs

    isRTL: boolean
    isIEMode: boolean
    isIE10Mode: boolean
    layoutNavbarBg: string
    layoutSidenavBg: string
    layoutFooterBg: string

}
