import { NounInfo, NounInfoRaw } from "./nounInfo";
import { VerbInfo, VerbInfoRaw } from "./verbInfo";

export type AdditionalInfoRaw = NounInfoRaw | VerbInfoRaw
