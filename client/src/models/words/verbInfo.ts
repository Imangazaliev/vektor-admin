import { TextItem } from "../shared/textItem";
import { VerbRootLetterCount } from "@/models/revisions/verbRootLetterCount";
import { VerbHarakah } from "@/models/words/verbHarakah";

export class VerbInfo {
    rootLetterCount!: number
    mudariLetters!: VerbHarakah[]
    verbFormNumber!: number
    masdars!: TextItem[]
    isMudaaf!: boolean
}

export class VerbInfoRaw {
    rootLetterCount!: VerbRootLetterCount
    mudariLetters!: VerbHarakah[]
    verbFormNumber!: number
    masdars!: string[]
    isMudaaf!: boolean
}
