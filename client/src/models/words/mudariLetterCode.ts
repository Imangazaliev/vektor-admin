export enum MudariLetterCode {
    FATHA = "a",
    KASRA = "i",
    DAMMA = "u"
}
