import { WordTranslation } from "../revisions/wordTranslation";
import { Idiom } from "../revisions/idiomSample";
import { WordType } from "../../enums/revisions/wordType";
import { NounInfoRaw } from "./nounInfo";
import { VerbInfoRaw } from "@/models/words/verbInfo";

export class WordInfo {
    arabicText!: string
    rootWord!: string
    type!: WordType | null
    additionalInfo!: AdditionalInfo | null
    newType!: string
    otherWordForms!: string[]
    lexicalComment!: string | null
    translationSearchKeywords!: string[]
    translations: WordTranslation[] = []
    idioms: Idiom[] = []
    translationRawText!: string
    additionalInfoRawText!: string
}

type AdditionalInfo = NounInfoRaw | VerbInfoRaw
