import { TextItem } from "../shared/textItem";

export class NounInfo {
    isFeminine!: boolean
    feminineForm!: string | null
    pluralForms!: TextItem[]
}

export class NounInfoRaw {
    isFeminine!: boolean
    feminineForm!: string | null
    pluralForms!: string[]
}
