import { MudariLetterCode } from "@/models/words/mudariLetterCode";

export class VerbHarakah {
    id!: number;
    pastTime!: MudariLetterCode | null;
    presentTime!: MudariLetterCode;
}
