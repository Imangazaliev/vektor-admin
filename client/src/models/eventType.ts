export enum EventType {
    WORD_CREATED = 'word_created',
    WORD_DELETED = 'word_deleted',
    WORD_STATUS_CHANGED = 'word_status_changed',
    REVISION_STATUS_CHANGED = 'revision_status_changed',
}