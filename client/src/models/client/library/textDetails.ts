import { DifficultyLevel } from "../../library/editor/difficultyLevel";
import { TextParagraph } from "../../library/editor/textParagraph";

export class TextDetails {
    id!: number;
    name!: string;
    categoryName!: string;
    categoryId!: number;
    difficulty!: DifficultyLevel;
    lexicalComment!: string;
    createdAt!: Date;
    paragraphs!: TextParagraph[]
}
