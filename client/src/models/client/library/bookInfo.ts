export class BookInfo {
    id!: number;
    name!: string;
    categoryId!: number;
    authorName!: number;
    difficulty!: string;
    createdAt!: string;
}
