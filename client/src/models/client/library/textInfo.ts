import { DifficultyLevel } from "../../library/editor/difficultyLevel";

export class TextInfo {
    id!: number;
    name!: string;
    categoryName!: string;
    categoryId!: number;
    difficulty!: DifficultyLevel;
    lexicalComment!: string;
    isDraft!: boolean;
    createdAt!: Date
}
