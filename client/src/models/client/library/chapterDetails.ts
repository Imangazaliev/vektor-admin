import { TextParagraph } from "../../library/editor/textParagraph";

export class ChapterDetails {
    id!: number;
    name!: string;
    bookName!: string;
    paragraphs!: TextParagraph[]
}
