import { DifficultyLevel } from "../../library/editor/difficultyLevel";
import { RawToc } from "@/models/library/editor/books/toc/raw/rawToc";

export class BookDetails {
    id!: number;
    name!: string;
    slug!: string;
    categoryName!: string;
    categoryId!: number;
    difficulty!: DifficultyLevel;
    description!: string;
    createdAt!: Date;
    toc!: RawToc;
}
