export class UserProfile {
    id!: number
    userName!: string
    role!: string
    email!: string
    permissions!: string[]
}
