export class TextItem {
    text!: string
}

export function wrapItems(items: string[] | null): TextItem[] {
    if (!items) return []
    return items.map((item: string) => ({ text: item }))
}

export function unwrapItems(items: TextItem[] | null): string[] {
    if (!items) return []
    return items.map((item: TextItem) => item.text)
}
