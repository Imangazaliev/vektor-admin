import { TextParagraph } from "../textParagraph";
import { TextParagraphBlock } from "../textParagraphBlock";

export class Text {
    id!: number;
    name!: string;
    slug!: string;
    categoryId!: number;
    difficulty!: string;
    paragraphs!: TextParagraph[]
    lexicalComment!: Array<TextParagraphBlock> | null;
    isDraft!: boolean;
    createdAt!: string;
    updatedAt!: string;
}
