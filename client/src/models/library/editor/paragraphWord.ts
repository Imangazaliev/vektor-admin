export class ParagraphWord {
    arabicText!: string;
    translationText!: string;
    order!: number;
}
