export enum DifficultyLevel {
    Beginner = "beginner",
    Basic = "basic",
    Middle = "middle",
    Hard = "hard"
}
