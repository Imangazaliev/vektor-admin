import { TextParagraphBlock } from "./textParagraphBlock";
import { ParagraphWord } from "./paragraphWord";

export class TextParagraph {
    id!: number;
    arabicText!: TextParagraphBlock[];
    audioUrl!: string | null;
    audioId!: string | null;
    translationText!: TextParagraphBlock[];
    words!: ParagraphWord[];
    order!: number;
}
