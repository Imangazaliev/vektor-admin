export class Book {
    id!: number;
    name!: string;
    categoryId!: number;
    authorName!: number;
    difficulty!: string;
    createdAt!: string;
    updatedAt!: string;
}
