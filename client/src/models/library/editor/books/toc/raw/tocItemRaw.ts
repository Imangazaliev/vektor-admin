import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";

export abstract class TocItemRaw {
    abstract type: TocItemType;
    id!: number | null;
    uuid!: string;
    name!: string;
    slug!: string;
    parentUuid!: string | null;
    order!: number;
}
