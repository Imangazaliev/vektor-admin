import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";
import { TocItemRaw } from "@/models/library/editor/books/toc/raw/tocItemRaw";

export class TocChapterRaw extends TocItemRaw {
    type: TocItemType = TocItemType.CHAPTER;
}
