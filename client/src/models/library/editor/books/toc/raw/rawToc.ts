import { TocSectionRaw } from "@/models/library/editor/books/toc/raw/tocSectionRaw";
import { TocChapterRaw } from "@/models/library/editor/books/toc/raw/tocChapterRaw";

export class RawToc {
    sections!: TocSectionRaw[];
    chapters!: TocChapterRaw[];
}
