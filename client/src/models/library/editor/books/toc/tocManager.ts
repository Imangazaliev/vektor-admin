import { TocItem } from "@/models/library/editor/books/toc/tocItem";
import { TocSection } from "@/models/library/editor/books/toc/tocSection";
import { TocChapter } from "@/models/library/editor/books/toc/tocChapter";
import { createModel } from "@/mixins/functions/createModel";
import { v4 as uuid } from "uuid";
import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";
import { RawToc } from "@/models/library/editor/books/toc/raw/rawToc";
import { TocItemRaw } from "@/models/library/editor/books/toc/raw/tocItemRaw";
import { TocSectionRaw } from "@/models/library/editor/books/toc/raw/tocSectionRaw";
import { TocChapterRaw } from "@/models/library/editor/books/toc/raw/tocChapterRaw";
import Vue from "vue";

export class TocManager {

    tocItems: TocItem[] = []
    private flatToc: Map<string, TocItem> = new Map()

    setRawToc(tocRawData: RawToc) {
        const sections = tocRawData.sections.map(item => ({
            //@ts-ignore
            type: TocItemType.SECTION,
            isCollapsed: true,
            ...item
        }))
        //@ts-ignore
        const chapters = tocRawData.chapters.map(item => ({ type: TocItemType.CHAPTER, ...item }))
        const allNodes = (sections as TocItemRaw[])
            .concat(chapters)
            .sort(function(a, b) {
               // const uuidCompare = a.parentUuid && b.parentUuid ? a.parentUuid.localeCompare(b.parentUuid) : null
                const orderCompare = a.order - b.order
                //return uuidCompare || orderCompare;
                return orderCompare;
            });
        this.tocItems = this.buildTree(allNodes, null, 0)!!
        this.flatToc = new Map()
        this.addItemsToFlatToc(this.flatToc, this.tocItems)
    }

    getRawToc(): RawToc {
        const tocItems = Array.from(this.flatToc.values())
        const sections = tocItems.filter(item => this.isSection(item))
            .map(item => createModel(TocSectionRaw, {
                id: item.id,
                type: TocItemType.SECTION,
                uuid: item.uuid,
                parentUuid: item.parent?.uuid || null,
                order: item.order,
                name: item.name,
                slug: item.slug
            }))
        const chapters = tocItems.filter(item => !this.isSection(item))
            .map(item => createModel(TocChapterRaw, {
                type: item.type,
                id: item.id,
                uuid: item.uuid,
                parentUuid: item.parent?.uuid || null,
                order: item.order,
                name: item.name,
                slug: item.slug
            }))
        return createModel(RawToc, { sections, chapters })
    }

    addChapter(parent: TocSection | null): TocChapter {
        const chapter = createModel(TocChapter, {
            uuid: uuid(),
            parent: parent
        })
        this.addItem(parent, chapter)
        return chapter
    }

    addSection(parent: TocSection | null): TocSection {
        const section = createModel(TocSection, {
            uuid: uuid(),
            parent: parent,
            children: []
        })
        this.addItem(parent, section)
        return section
    }

    saveItem(item: TocItem) {
        let updatedTocItem: TocItem
        if (this.isSection(item)) {
            updatedTocItem = createModel(TocSection, { ...item })
        } else {
            updatedTocItem = createModel(TocChapter, { ...item })
        }
        this.updateItem(updatedTocItem)
    }

    updateItem(updatedItem: TocItem) {
        this.flatToc.set(updatedItem.uuid, updatedItem)
    }

    deleteItem(removedItem: TocItem) {
        let parentList: TocItem[]
        if (removedItem.parent) {
            const parentSection = this.flatToc.get(removedItem.parent.uuid) as TocSection
            parentList = parentSection.children
        } else {
            parentList = this.tocItems
        }
        const itemIndex = parentList.findIndex(item => item.uuid === removedItem.uuid)
        parentList.splice(itemIndex, 1)
        this.flatToc.delete(removedItem.uuid)

        if (this.isSection(removedItem)) {
            this.deleteChildItems((removedItem as TocSection).children)
        }
    }

    getItemByPath(path: number[]): TocItem {
        const startParentIndex = path[0]
        const startItem = this.tocItems[startParentIndex]
        if (path.length > 1) {
            return this.getItemByPathInternal(path, 1, startItem as TocSection)
        } else {
            return startItem
        }
    }

    isSection(item: TocItem): boolean {
        return item.type == TocItemType.SECTION
    }

    private buildTree(
        elements: TocItemRaw[],
        parent: TocSection | null,
        depth: number
    ): TocItem[] | null {
        const branch: TocItem[] = [];
        elements.forEach((element: TocItemRaw) => {
            if (element?.parentUuid == parent?.uuid) {
                if (element.type == TocItemType.SECTION) {
                    const sectionRaw = element as TocChapterRaw
                    const section = createModel(TocSection, {
                        id: sectionRaw.id,
                        type: TocItemType.SECTION,
                        uuid: sectionRaw.uuid,
                        parent: parent,
                        order: sectionRaw.order,
                        name: sectionRaw.name,
                        slug: sectionRaw.slug,
                        depth: depth,
                        isCollapsed: false
                    })
                    const children = this.buildTree(elements, section, depth + 1);
                    section.children = children || []
                    branch.push(section);
                } else {
                    const chapterRaw = element as TocChapterRaw
                    const chapter = createModel(TocChapter, {
                        id: chapterRaw.id,
                        type: TocItemType.CHAPTER,
                        uuid: chapterRaw.uuid,
                        parent: parent,
                        order: chapterRaw.order,
                        name: chapterRaw.name,
                        slug: chapterRaw.slug,
                        depth: depth
                    })
                    branch.push(chapter);
                }
            }
        })

        return branch;
    }

    private addItemsToFlatToc(flatToc: Map<string, TocItem>, tocItems: TocItem[]) {
        tocItems.forEach((item: TocItem) => {
            if (item.type == TocItemType.SECTION) {
                const section = item as TocSection
                flatToc.set(section.uuid, section)
                if (section.children) {
                    this.addItemsToFlatToc(flatToc, section.children)
                }
            } else {
                flatToc.set(item.uuid, item)
            }
        })
    }

    private addItem(parent: TocSection | null, item: TocItem) {
        let parentList: TocItem[]
        if (parent) {
            const section = this.flatToc.get(parent.uuid) as TocSection
            parentList = section.children
        } else {
            parentList = this.tocItems
        }
        item.order = parentList.length
        item.parent = parent
        parentList.push(item)
        if (parent) {
            Vue.set(parent, "children", parentList)
        }
        this.updateItem(item)
    }

    private deleteChildItems(items: TocItem[]) {
        items.forEach(item => {
            this.deleteItem(item)
            if (this.isSection(item)) {
                this.deleteChildItems((item as TocSection).children)
            }
        })
    }

    private getItemByPathInternal(
        path: number[],
        currentDepth: number,
        parentItem: TocSection
    ): TocItem {
        const itemIndex = path[currentDepth]
        const subItem = parentItem.children[itemIndex]
        const isLastItem = currentDepth == path.length - 1
        if (isLastItem) {
            return subItem
        } else {
            return this.getItemByPathInternal(path, currentDepth + 1, subItem as TocSection)
        }
    }

}


