import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";
import { TocSection } from "@/models/library/editor/books/toc/tocSection";

export abstract class TocItem {
    abstract type: TocItemType;
    id!: number | null;
    uuid!: string;
    parent!: TocSection | null;
    order!: number;
    name!: string;
    slug!: string;
    depth!: number
}

