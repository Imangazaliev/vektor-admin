import { TocItem } from "@/models/library/editor/books/toc/tocItem";
import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";

export class TocChapter extends TocItem {
    type: TocItemType = TocItemType.CHAPTER;
}
