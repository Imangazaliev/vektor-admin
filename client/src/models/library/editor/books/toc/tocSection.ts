import { TocItem} from "@/models/library/editor/books/toc/tocItem";
import { TocItemType } from "@/models/library/editor/books/toc/tocItemType";

export class TocSection extends TocItem {
    type: TocItemType = TocItemType.SECTION;
    isCollapsed: boolean = false;
    children!: TocItem[];
}
