export enum RevisionStatus {
    Opened = "opened",
    Correction = "on_correction",
    Rejected = "rejected",
    Accepted = "accepted"
}
