import { TranslationSample } from "./translationSample";

export class WordTranslation {
    id!: number
    text!: string
    sortOrder!: number
    isSamplesAdded!: boolean
    samples!: TranslationSample[]
}
