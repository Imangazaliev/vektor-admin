import axios from 'axios'

axios.defaults.withCredentials = true

export const baseUrl = process.env.NODE_ENV === 'development' ? "http://localhost:3000" : "https://kalimat.su"

const http = axios.create({
    baseURL: `${baseUrl}/api`,
    withCredentials: true,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})

export default http
