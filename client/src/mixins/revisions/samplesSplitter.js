const sampleRegex = /(\s?[~]?\s?[\u0621-\u064A\u064e\u0650\u064f\u064b\u064d\u064c\u0652\u0651]\s?[~]?\s?(или)?\s?)+/gi

function splitSamples(translationRawText, originalWord) {
    const translationsRawText = translationRawText.split('*')[0]
    return splitSamplesList(translationsRawText)
        .map(translationText => extractSamples(translationText, originalWord))
}

function splitSamplesList(translationRawText) {
    const firstSymbols = translationRawText.substring(0, 2)
    const isListWithDots = firstSymbols === '1.'
    const isListWithBrackets = firstSymbols === '1)'
    if (!isListWithDots && !isListWithBrackets) {
        return [translationRawText]
    }

    let translations
    if (isListWithDots) {
        translations = translationRawText.split(/\d+\s*?\./g)
    } else {
        translations = translationRawText.split(/\d+\s*?\)/g)
    }
    return translations
        .map(item => item.trim())
        .filter(item => item !== '')
}

function extractTranslations(translationText) {
    const parts = translationText.split(';')
    const translations = parts
        .filter(item => !item.match(sampleRegex))
        .map(item => item.trim())
        .join('; ')

    return translations
}

function extractSamples(translationText, originalWord) {
    const parts = translationText.split(';')
    const removeHarakas = (text) => text.replace(/[\u202C\u064B\u064C\u064E-\u0652]/gi, '')
    const correctTilda = (text) => {
        if (text[0] === '~') {
            return text.substring(1) + '~'
        } else {
            return text
        }
    }
    const samples = parts
        .filter(item => item.match(sampleRegex))
        .map(item => item.trim())
        .map((item, index) => {
            const rawArabicText = item.match(sampleRegex)[0].trim()
            const arabicText = correctTilda(rawArabicText) //move tilda at the end of text
                .replace('~', removeHarakas(originalWord)) //replace tilda with original word
                .replace('или', '||')
                .replace(ARABIC_TATWEEL, "")
            const translationText = item.substring(rawArabicText.length).trim()
            return { sortOrder: index + 1, arabicText, translationText }
        })

    return samples
}

function splitIdioms(translationRawText, originalWord) {
    const idiomsText = translationRawText.split('*')[1].trim()
    return extractSamples(idiomsText, originalWord)
}

export function removeTatweels(text) {
    console.log("removeTatweels: ", text, Array.isArray(text))
    if (Array.isArray(text)) {
        return text.map(item => item.replaceAll(ARABIC_TATWEEL, ""))
    }
    return text.replaceAll(ARABIC_TATWEEL, "")
}

export var ARABIC_TATWEEL = 'ـ'

export default {
    splitSamples,
    splitIdioms,
    removeTatweels
}
