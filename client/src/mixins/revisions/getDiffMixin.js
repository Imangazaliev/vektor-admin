import * as Diff from "diff"

export default {
    methods: {
        getDiffLines(one, other) {
            return this.getDiff(one, other, 'lines')
        },
        getDiffArray(one, other) {
            return this.getDiff(one, other, 'array')
        },
        getDiff(one, other, diffType = 'words') {
            let diff
            let textOne = one ? String(one) : ''
            let textTwo = other ? String(other) : ''

            switch (diffType) {
                case 'array':
                    diff = Diff.diffArrays(one || [], other || [])
                    break
                case 'words':
                    diff = Diff.diffWords(textOne, textTwo)
                    break
                case 'lines':
                    diff = Diff.diffLines(textOne, textTwo)
                    break
                case 'chars':
                    diff = Diff.diffChars(textOne, textTwo)
                    break
                default:
                    diff = Diff.diffChars(textOne, textTwo)
            }
            const diffContainer = document.createElement("span")
            let color = ''
            let span = null
            diff.forEach(function (part) {
                // green for additions, red for deletions
                color = part.added ? '#cdffd8' :
                    part.removed ? '#ffdce0' : 'transparent'
                span = document.createElement('span')
                span.style.background = color
                span.style.padding = '3px'
                span.appendChild(document
                    .createTextNode(part.value))
                diffContainer.appendChild(span)
            })

            // make new element, insert document fragment, then get innerHTML!
            const div = document.createElement('div')
            div.appendChild(diffContainer.cloneNode(true))

            return div.innerHTML
        },
        isBothNull(one, two) {
            return (one === null || one === undefined) && (two === null || two === undefined)
        },
        isObjectsEqual(a, b) {
            if (a === b) return true
            if (a == null || b == null) return false
            return a === b
        },
        isArraysEqual(a, b) {
            if (a === b) return true
            if (a == null || b == null) return false
            if (a.length !== b.length) return false

            // If you don't care about the order of the elements inside
            // the array, you should sort both arrays here.
            // Please note that calling sort on an array will modify that array.
            // you might want to clone your array first.

            for (let i = 0; i < a.length; ++i) {
                if (a[i] !== b[i]) return false
            }
            return true
        }
    }
}
