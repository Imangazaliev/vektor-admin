import Component from "vue-class-component";
import Vue from "vue";

@Component
export class LibraryMixins extends Vue {

    difficultyLevels = {
        beginner: 'Начальный',
        basic: 'Базовый',
        middle: 'Средний',
        hard: 'Сложный'
    };

    get difficultyLevelOptions() {
        const levels = this.difficultyLevels
        return Object.keys(levels).map(key => ({
            //@ts-ignore
            title: levels[key], value: key
        }))
    }

}
