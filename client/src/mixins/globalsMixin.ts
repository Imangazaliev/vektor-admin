import Vue from "vue"
import HelperFuncs from "./helpersTs";
import { Component } from "vue-property-decorator";

@Component
export default class GlobalsMixin extends Vue {

    // Public url
    publicUrl: string | undefined = process.env.BASE_URL
    // Layout helpers
    layoutHelpers: HelperFuncs = new HelperFuncs()

    // Check for RTL layout
    get isRTL(): boolean {
        return document.documentElement.getAttribute('dir') === 'rtl' ||
            document.body.getAttribute('dir') === 'rtl'
    }

    // Check if IE
    get isIEMode(): boolean {
        // @ts-ignore
        return typeof document['documentMode'] === 'number'
    }

    // Check if IE10
    get isIE10Mode(): boolean {
        // @ts-ignore
        return this.isIEMode && document['documentMode'] === 10
    }

    // Layout navbar color
    get layoutNavbarBg(): string {
        return 'navbar-theme'
    }

    // Layout sidenav color
    get layoutSidenavBg(): string {
        return 'sidenav-theme'
    }

    // Layout footer color
    get layoutFooterBg(): string {
        return 'footer-theme'
    }

    // Animate scrollTop
    scrollTop(to: number, duration: number, element = document.scrollingElement || document.documentElement) {
        if (element.scrollTop === to) return
        const start = element.scrollTop
        const change = to - start
        const startDate = +new Date()

        // t = current time; b = start value; c = change in value; d = duration
        const easeInOutQuad = (t: number, b: number, c: number, d: number): number => {
            t /= d / 2
            if (t < 1) return c / 2 * t * t + b
            t--
            return -c / 2 * (t * (t - 2) - 1) + b
        }

        const animateScroll = () => {
            const currentDate: number = +new Date()
            const currentTime: number = currentDate - startDate
            element.scrollTop = easeInOutQuad(currentTime, start, change, duration)
            if (currentTime < duration) {
                requestAnimationFrame(animateScroll)
            } else {
                element.scrollTop = to
            }
        }

        animateScroll()
    }

}
