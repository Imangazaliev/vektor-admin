export default {
    data: () => ({
        wordTypes: {
            default: 'Не выбрано',
            noun: 'Имя (исм)',
            verb: 'Глагол (фи`ль)',
            particle: 'Частица (харф)'
        },
        wordStatuses: {
            raw: 'Не готово',
            formatted: 'Отформатировано',
            commission_verified: 'Проверено коммисией'
        },
        mudariLetters: [
            { title: 'А - Фатха', code: 'a' },
            { title: 'И - Кясра', code: 'i' },
            { title: 'У - Дамма', code: 'u' }
        ],
        verbNumbers: [
            { title: 'I', number: 1 },
            { title: 'II', number: 2 },
            { title: 'III', number: 3 },
            { title: 'IV', number: 4 },
            { title: 'V', number: 5 },
            { title: 'VI', number: 6 },
            { title: 'VII', number: 7 },
            { title: 'VIII', number: 8 },
            { title: 'IX', number: 9 },
            { title: 'X', number: 10 },
            { title: 'XI', number: 11 },
            { title: 'XII', number: 12 },
            { title: 'XIII', number: 13 },
            { title: 'XIV', number: 14 },
            { title: 'XV', number: 15 }
        ],
        verbRootLetterCountOptions: [
            { title: '3 буквы (сулясий)', number: 3 },
            { title: '4 буквы (руба\'ий', number: 4 }
        ]
    }),
    methods: {
        isWordTypeExists(type) {
            return Object.keys(this.wordTypes).find(key => key === type) != null
        },
        getMudariLetter(code) {
            const letter = this.mudariLetters.find(item => item.code === code)
            return letter ? letter.title : null
        },
        createOptions(options) {
            return Object.keys(options).map(key => ({
                title: options[key],
                code: key
            }))
        },
        addDefaultOption(options, keyName = 'code') {
            return [{ title: 'Не выбрано', [keyName]: null }, ...options]
        }
    }
}
