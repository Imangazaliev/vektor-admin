import Component from "vue-class-component";
import Vue from "vue";
import { VerbHarakah } from "@/models/words/verbHarakah";

export enum WordStatus {
    RAW = "raw",
    FORMATTED = "formatted",
    COMMISSION_VERIFIED = "commission_verified"
}

@Component
export class WordParamTitlesMixin extends Vue {

    wordTypes = {
        noun: 'Имя (исм)',
        verb: 'Глагол (фи`ль)',
        particle: 'Частица (харф)'
    }
    wordStatuses = {
        raw: 'Не готово',
        formatted: 'Отформатировано',
        commission_verified: 'Проверено коммисией'
    }
    harakaTitles = [
        { title: 'А - Фатха', code: 'a' },
        { title: 'И - Кясра', code: 'i' },
        { title: 'У - Дамма', code: 'u' }
    ]
    harakas = [
        { title: 'А', code: 'a' },
        { title: 'И', code: 'i' },
        { title: 'У', code: 'u' }
    ]
    verbNumbers = [
        { title: 'I', number: 1 },
        { title: 'II', number: 2 },
        { title: 'III', number: 3 },
        { title: 'IV', number: 4 },
        { title: 'V', number: 5 },
        { title: 'VI', number: 6 },
        { title: 'VII', number: 7 },
        { title: 'VIII', number: 8 },
        { title: 'IX', number: 9 },
        { title: 'X', number: 10 },
        { title: 'XI', number: 11 },
        { title: 'XII', number: 12 },
        { title: 'XIII', number: 13 },
        { title: 'XIV', number: 14 },
        { title: 'XV', number: 15 }
    ]

    verbRootLetterCountOptions = [
        { title: '3 буквы (сулясий)', number: 3 },
        { title: '4 буквы (руба\'ий', number: 4 }
    ]


    isWordTypeExists(type: string | null) {
        if (!type) return false;
        return Object.keys(this.wordTypes).find(key => key === type) != null
    }

    getHarakaTitle(code: string): string {
        const letter = this.harakaTitles.find(item => item.code === code)
        return letter!!.title
    }

    getHarakahByCode(code: string): string {
        const letter = this.harakas.find(item => item.code === code)
        return letter!!.title
    }

    getVerbHarakahAsString(letter: VerbHarakah): string {
        var letterRepresentation = ""
        if (letter.pastTime) {
            letterRepresentation = this.getHarakahByCode(letter.pastTime) + "/"
        }
        letterRepresentation += this.getHarakahByCode(letter.presentTime)
        return letterRepresentation
    }

    getRomanNumber(number: number): string {
        const letter = this.verbNumbers.find(item => item.number === number)
        return letter!!.title
    }

    createOptions(options: any) {
        return Object.keys(options).map((key: string) => ({
            title: options[key],
            code: key
        }))
    }

    addDefaultOption(options: any, keyName = 'code') {
        return [{ title: 'Не выбрано', [keyName]: null }, ...options]
    }

}
