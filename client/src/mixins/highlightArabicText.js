const ARABIC_VOWELS_REGEXP = "[" +
    "\\u064b\u064c\\u064d\\u064e\\u064f\\u0650\\u0651\\u0652" +
    "\\u0653\\u06e1\\u08f0\\u0670" +
    "]*"
const ARABIC_REGEXP = "[\\p{InARABIC}]+((\\s*~)*(\\s*[\\p{InARABIC}]+)+)*"

const ANY_ALIF_REGEXP = /[\u0622\u0623\u0625\u0627\u0671]/
const ANY_ALIF_REGEXP_LITERAL = "[\\\u0622\\\u0623\\\u0625\\\u0627\\\u0671]"
const ANY_WAW_REGEXP = /[\u0624\u0648]/gi
const ANY_WAW_REGEXP_LITERAL = "[\\\u0624\\\u0648]"
const ANY_YEH_REGEXP = /[\u0626\u0649]/gi
const ANY_YEH_REGEXP_LITERAL = "[\\\u0626\\\u0649]"

export default {
    methods: {
        highlightArabicText(text) {
            const arabicTextRegex = /(([\u0621-\u064A\u064e\u0650\u064f\u064b\u064d\u064c\u0652\u0651]+)|([~]+))+/ig
            return text.replace(arabicTextRegex, '<span style="color: #26B4FF">$&</span>')
        },
        highlightSearchMatches(text) {
            let regexText = "";
            [...text].forEach(element => {
                regexText = regexText + element + ARABIC_VOWELS_REGEXP
            })

            /*
            regex = regex.replace(new RegExp(ANY_ALIF_REGEXP, "gi"), ANY_ALIF_REGEXP_LITERAL)
                        .replace(new RegExp(ANY_WAW_REGEXP, "gi"), ANY_WAW_REGEXP_LITERAL)
                        .replace(new RegExp(ANY_YEH_REGEXP, "gi"), ANY_YEH_REGEXP_LITERAL)
            */
            const regex = new RegExp(regexText, "gi")
            return text.replace(regex, '<span style="background-color: yellow;">$&</span>')
        }
    }
}
