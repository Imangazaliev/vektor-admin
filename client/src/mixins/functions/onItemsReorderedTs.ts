import Component from "vue-class-component";
import Vue from "vue";

@Component
export class OnItemsReordered extends Vue {
    onItemsReordered(items: any[], keyName: string) {
        items.forEach((item, index) => item[keyName] = index + 1)
    }
}
