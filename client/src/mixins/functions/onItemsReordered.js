export default {
    methods: {
        onItemsReordered(items, keyName) {
            items.forEach((item, index) => item[keyName] = index + 1)
        }
    }
}
