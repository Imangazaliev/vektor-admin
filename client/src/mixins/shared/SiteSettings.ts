import Component from "vue-class-component";
import Vue from "vue";
import { SiteSettings } from "@/enums/shared/siteSettings";

@Component
export class SiteSettingsMixins extends Vue {

    getSiteSettings(): SiteSettings {
        return this.$store.state.settings.siteSettings
    }

}
