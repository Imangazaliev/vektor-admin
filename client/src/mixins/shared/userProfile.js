export default {
    methods: {
        getUser() {
            return this.$store.state.user.profile
        },
        hasPermission(permissionCode) {
            const profile = this.$store.state.user.profile
            const permissions = profile ? profile.permissions : null
            return permissions ? permissions.includes(permissionCode) : false
        },
        checkItemPermission(item) {
            if (item.permission === undefined) return true

            if (Array.isArray(item.permission)) {
                for (const permission of item.permission) {
                    if (this.hasPermission(permission)) return true
                }
                return false
            } else {
                return this.hasPermission(item.permission)
            }
        }
    }
}
