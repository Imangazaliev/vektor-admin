import Component from "vue-class-component"
import Vue from "vue"

@Component
export class FormatDateMixins extends Vue {

    formatDate(date: string) {
        return new Date(date)
            .toLocaleDateString("ru", {
                year: 'numeric',
                month: '2-digit',
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit"
            })
    }

}
