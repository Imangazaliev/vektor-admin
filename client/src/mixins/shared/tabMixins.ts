import Component from "vue-class-component";
import Vue from "vue";

@Component
export class TabsMixins extends Vue {

    bindTabs(
        tabIndexVarName: string,
        tabs: string[],
        tabQueryVar: string,
        defaultTab: string | null = null
    ) {
        // @ts-ignore
        const changeListener = () => [this[tabIndexVarName]];
        this.$watch(changeListener, val => {
            const tabCode = tabs[+val[0]] as string | undefined
            this.addQueryParam(tabQueryVar, tabCode)
        })

        this.$nextTick(() => {
            const tabCode = this.$route.query[tabQueryVar] as string | undefined
            if (!tabCode) {
                // @ts-ignore
                this[tabIndexVarName] = defaultTab || 0
                return
            }
            // @ts-ignore
            this[tabIndexVarName] = tabs.indexOf(tabCode) || tabs.indexOf(tabCode)
        })
    }

}
