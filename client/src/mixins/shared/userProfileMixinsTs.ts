import Component from "vue-class-component";
import Vue from "vue";
import { UserProfile } from "../../models/shared/userProfile";
import { PermissionCode } from "@/enums/shared/permissionCode";

@Component
export class UserProfileMixins extends Vue {

    getUser(): UserProfile {
        return this.$store.state.user.profile
    }

    hasPermission(permissionCode: PermissionCode): boolean {
        const profile = this.getUser();
        const permissions = profile ? profile.permissions : null;
        return permissions ? permissions.includes(permissionCode) : false
    }

    checkItemPermission(item: any): boolean {
        if (item.permission === undefined) return true
        //console.log(item.permission + ": ", this.hasPermission(item.permission))

        if (Array.isArray(item.permission)) {
            return item.permission.some((permission: PermissionCode) => this.hasPermission(permission))
        } else {
            return this.hasPermission(item.permission)
        }
    }

}
