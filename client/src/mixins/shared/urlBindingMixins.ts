import Component from "vue-class-component";
import Vue from "vue";

@Component
export class UrlBindingMixins extends Vue {

    bindQueryParam(
        varPath: string,
        queryParamName: string,
        defaultValue: string | null = null
    ) {
        const varName = this.getVarName(varPath);
        const parentObject = this.getParentObject(varPath)
        // @ts-ignore
        const changeListener = () => [parentObject[varName]];
        this.$watch(changeListener, (newValue: string[]) => {
            this.addQueryParam(queryParamName, newValue[0])
        })

        this.$nextTick(() => {
            const queryValue = this.$route.query[queryParamName] as string | undefined
            // @ts-ignore
            parentObject[varName] = queryValue || defaultValue
        })
    }

    private getVarName(path: string): string {
        return path.includes(".") ? path.split('.').pop()!! : path
    }

    private getParentObject(pathString: string) : any {
        let schema: any = this;  // a moving reference to internal objects within obj
        const path = pathString.split('.');
        const len = path.length;
        for(let i = 0; i < len-1; i++) {
            let elem = path[i];
            if( !schema[elem] ) schema[elem] = {}
            schema = schema[elem];
        }
        return schema;
    }

}
