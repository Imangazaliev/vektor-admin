export default {
    methods: {
        formatDate(date) {
            return new Date(date)
                .toLocaleDateString("ru", {
                    year: 'numeric',
                    month: '2-digit',
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                })
        }
    }
}
