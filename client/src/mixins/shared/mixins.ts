import Vue from "vue";
import Component from "vue-class-component";

@Component
export default class Mixin extends Vue {

    showSuccessNotification(title: string, message: string) {
        this.$notify({
            group: 'notifications-default',
            type: 'success',
            title: title,
            text: message
        })
    }

    showSuccessToast({ text }: { text: string }) {
        this.showToast({ text, type: 'success', icon: 'check' })
    }

    showErrorToast({ text }: { text: string }) {
        this.showToast({ text, type: 'error', icon: 'check' })
    }

    showToast(
        {
            text,
            type = undefined,
            icon = undefined
        }: {
            text: string,
            type?: string,
            icon?: string
        }
    ) {
        this.$toasted.show(text, {
            theme: 'bubble',
            iconPack: 'fontawesome',
            icon: icon,
            duration: 3000,
            type: type
        })
    }

    addDefaultOption(options: any[], keyName = 'code') {
        return [{ title: 'Не выбрано', [keyName]: null }, ...options]
    }

    addQueryParam(parameter: string, value: string | undefined) {
        let query: any = this.$route.query
        if (value == undefined || value == "") {
            query = { ...query, [parameter]: undefined }
        } else {
            query = { ...query, [parameter]: value }
        }
        this.$router.push({ path: this.$route.path, query })
    }

}
