/**
 * Проверяет текст на соответствие требованиям. Валидные примеры:
 * - текст
 * - "текст ..." или "текст ... текст" или "... текст ..."
 * - текст (текст)
 * - текст || текст
 */
export const isArabicSample = function (value) {
    return /([\u0600-\u06FF]+)(\s|\s\|\|\s)?(( ...)?|[()!]?)*$/.test(value)
}

export const isArabic = function (value) {
    return /([\u0600-\u06FF]+)(\s[\u0600-\u06FF])*$/.test(value)
}

export default {
    methods: {
        getFieldState(field) {
            return field.$invalid ? 'invalid' : null
        }
    }
}
