/**
 * Проверяет текст на соответствие требованиям. Валидные примеры:
 * - текст
 * - "текст ..." или "текст ... текст" или "... текст ..."
 * - текст (текст)
 * - текст || текст
 */
import Component from "vue-class-component";
import Vue from "vue";

export function isArabicSample(value: string) {
    return /([\u0600-\u06FF]+)(\s|\s\|\|\s)?(( ...)?|[()!]?)*$/.test(value)
}

export function isSlugFormat(value: string): boolean {
    return /^[a-z0-9]+(?:-[a-z0-9]+)*$/.test(value)
}

export function  isArabic(value: string): boolean {
    return /([\u0600-\u06FF]+)(\s[\u0600-\u06FF])*$/.test(value)
}

@Component
export class ValidationsMixin extends Vue {

    getFieldState(field: any) {
        return field.$invalid ? 'invalid' : null
    }

    isArabicSample = isArabicSample;

    removeHarakahs(arabicText: string) {
        return arabicText.replace(/[\u202C\u064B\u064C\u064E-\u0652]/gi, '');
    }



}
