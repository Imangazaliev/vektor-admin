import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import http from "@/httpService";
import { SET_USER_DATA } from "@/store/auth/userConstants";
import SiteSettings from "@/pages/admin/administration/siteSettings.vue";
import { SET_SITE_SETTINGS } from "@/store/settings/settingsConstants";

@Module
export class SiteSettingsModule extends VuexModule {

    siteSettings: SiteSettings | null = null;

    @Mutation
    [SET_SITE_SETTINGS](siteSettings: SiteSettings) {
        this.siteSettings = siteSettings
    }

}
