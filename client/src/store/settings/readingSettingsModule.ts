import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import { SET_ARABIC_SIZE, SET_TRANSLATION_SIZE } from "@/store/settings/fontConst"

@Module
export class ReadingSettingsModule extends VuexModule {

    arabicTextSize: number = 24;
    translationTextSize: number = 20;

    @Mutation
    [SET_ARABIC_SIZE](value: number) {
        this.arabicTextSize = value
    }
    @Mutation
    [SET_TRANSLATION_SIZE](value: number) {
        this.translationTextSize = value
    }


    @Action
    setArabicTextSize(value: number) {
        this.context.commit(SET_ARABIC_SIZE, value)
    }
    @Action
    setTranslationTextSize(value: number) {
        this.context.commit(SET_TRANSLATION_SIZE, value)
    }


}