import Vue from 'vue'
import Vuex from 'vuex'
import { UserModule } from './auth/userModule'
import { SiteSettingsModule } from './settings/siteSettingsModule'
import { ReadingSettingsModule } from "./settings/readingSettingsModule";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        user: UserModule,
        settings: SiteSettingsModule,
        readingSettings: ReadingSettingsModule

    },
    strict: debug,
})
