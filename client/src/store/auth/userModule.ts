import http from '../../httpService'

import {
    CLEAR_USER_DATA,
    SET_USER_DATA,
    SET_USER_STATUS
} from './userConstants'
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import { UserProfile } from "../../models/shared/userProfile";
import store from "@/store";
import { LoginStatus } from "@/store/auth/loginStatus";

@Module
export class UserModule extends VuexModule {

    userStatus?: string = undefined;
    profile: UserProfile | null = null;

    @Mutation
    [SET_USER_STATUS](status: string) {
        this.userStatus = status
    }

    @Mutation
    [SET_USER_DATA](user: any) {
        this.profile = user
    }

    @Mutation
    [CLEAR_USER_DATA]() {
        this.profile = null
    }

    get isAuthenticated(): boolean {
        return this.userStatus === LoginStatus.Authenticated
    }

    @Mutation
    private setLoggedIn(isLoggedIn: boolean) {
        localStorage.setItem('is_logged_in', JSON.stringify(isLoggedIn))
    }

    private get isLoggedIn(): boolean {
        return localStorage.getItem('is_logged_in') === 'true'
    }

    @Action({ rawError: true })
    async tryLogin({ email, password }: { email: string, password: string }) {
        this.context.commit(SET_USER_STATUS, LoginStatus.LoginInProgress);
        try {
            const response = await http.post('/admin/login', { email, password });
            const user = response.data;

            this.context.commit('setLoggedIn', true);
            this.context.commit(SET_USER_DATA, user);
            this.context.commit(SET_USER_STATUS, LoginStatus.Authenticated);
            return user
        } catch (error) {
            //console.log('Error.tryLogin: ', error);
            //this.context.dispatch('logoutUser');
            //throw error
        }
    }

    @Action({ rawError: true })
    public async logoutUser() {
        try {
            this.context.commit('setLoggedIn', false);
            await http.get('/admin/logout')
        } catch (error) {
            //console.log('Error.logoutUser: ', error)
            ///throw error
        } finally {

        }
        this.context.commit(SET_USER_STATUS, LoginStatus.Unauthenticated);
        this.context.commit(CLEAR_USER_DATA)
    }

    @Action({ rawError: true })
    public async fetchUserData() {
        if (!this.isLoggedIn) return

        this.context.commit(SET_USER_STATUS, LoginStatus.LoginInProgress);
        try {
            const response = await http.get('/admin/profile');
            const user = response.data;
            this.context.commit(SET_USER_DATA, user);
            this.context.commit(SET_USER_STATUS, LoginStatus.Authenticated)
        } catch (error) {
            this.context.commit('setLoggedIn', false);
            console.log('Error.fetchUserData: ', error);
            await this.context.dispatch('logoutUser')
        }
    }

}
