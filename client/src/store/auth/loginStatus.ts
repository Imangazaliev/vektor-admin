export enum LoginStatus {
    LoginInProgress = 'LOGIN_IN_PROGRESS',
    ProfileLoading = 'PROFILE_LOADING',
    Unauthenticated = 'UNAUTHENTICATED',
    Authenticated = 'AUTHENTICATED'
}
