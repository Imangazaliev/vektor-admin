import store from '../../store'

export var checkPassword = (to, from, next) => {
    const password = to.query.pass
    if (password === 'myPass') {
        next()
        return
    }
    next('/404')
}

export var ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/404')
}

export var ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/')
}

export function checkPermission(permissionCode) {
    return (to, from, next) => {
        const profile = store.state.user.profile
        const hasPermission = profile ? profile.permissions.includes(permissionCode) : false
        if (hasPermission) {
            next()
            return
        }
        next('/503')
    }
}
