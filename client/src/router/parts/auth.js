import LayoutBlank from '@/layout/base/LayoutBlank'

export default [
    {
        path: '/login',
        component: LayoutBlank,
        children: [
            {
                path: '/',
                component: () => import('@/pages/admin/auth/Login')
            }
        ],
    }
]
