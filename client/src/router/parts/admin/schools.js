import { ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/schools',
            component: () => import('@/pages/admin/schools/schoolsList')
        },
        {
            path: '/admin/schools/:id',
            component: () => import('@/pages/admin/schools/schoolInfo'),
            beforeEnter: ifAuthenticated
        },
    ]
}
