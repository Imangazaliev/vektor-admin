import multipleGuards from '../../helpers/multipleGuards'
import { checkPermission, ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/users',
            component: () => import('@/pages/admin/administration/usersList'),
            beforeEnter: checkPermission('users:manage')
        },
        {
            path: '/admin/roles',
            component: () => import('@/pages/admin/administration/rolesList'),
            beforeEnter: checkPermission('users:manage')
        },
        {
            path: '/admin/permissions',
            component: () => import('@/pages/admin/administration/permissionsList'),
            beforeEnter: checkPermission('users:manage')
        },
        {
            path: '/admin/settings',
            component: () => import('@/pages/admin/administration/siteSettings'),
            beforeEnter: checkPermission('admin-panel:settings')
        },
        {
            path: '/admin/permissions/:id',
            component: () => import('@/pages/admin/administration/permissionDetails'),
            beforeEnter: multipleGuards([ifAuthenticated, checkPermission('users:manage')])
        },
        {
            path: '/admin/roles/:id',
            component: () => import('@/pages/admin/administration/roleDetails'),
            beforeEnter: multipleGuards([ifAuthenticated, checkPermission('users:manage')])
        },
        {
            path: '/admin/users/:id',
            component: () => import('@/pages/admin/administration/userDetails'),
            beforeEnter: multipleGuards([ifAuthenticated, checkPermission('users:manage')])
        },
    ]
}
