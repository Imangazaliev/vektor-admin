import { ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/courses',
            component: () => import('@/pages/admin/courses/coursesList')
        },
        {
            path: '/admin/courses/:id',
            component: () => import('@/pages/admin/courses/courseInfo'),
            beforeEnter: ifAuthenticated
        },
    ]
}
