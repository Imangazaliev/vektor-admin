import { ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/students/:id',
            component: () => import('@/pages/admin/students/studentInfo'),
            beforeEnter: ifAuthenticated
        },
    ]
}
