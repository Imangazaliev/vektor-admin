import AdminLayout from '@/layout/AdminLayout'
import { ifAuthenticated } from '../../helpers/navGuards'
import administration from './administration'
import companies from './companies'
import clubs from './clubs'
import courses from './courses'
import schools from './schools'
import students from './students'

export default [
    {
        path: '/admin',
        component: AdminLayout,
        children: [
            {
                path: '',
                component: () => import('@/pages/admin/dashboard/dashboard')
            },
            {
                path: '/admin/rating',
                component: () => import('@/pages/admin/rating')
            },
            ...administration.items,
            ...companies.items,
            ...clubs.items,
            ...courses.items,
            ...schools.items,
            ...students.items,
        ],
        beforeEnter: ifAuthenticated
    }
]
