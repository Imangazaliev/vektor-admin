import { ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/companies',
            component: () => import('@/pages/admin/companies/companiesList')
        },
        {
            path: '/admin/schools/:id',
            component: () => import('@/pages/admin/companies/companyInfo'),
            beforeEnter: ifAuthenticated
        },
    ]
}
