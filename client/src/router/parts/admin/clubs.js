import { ifAuthenticated } from '../../helpers/navGuards'

export default {
    items: [
        {
            path: '/admin/clubs',
            component: () => import('@/pages/admin/clubs/clubsList')
        },
        {
            path: '/admin/clubs/:id',
            component: () => import('@/pages/admin/clubs/clubInfo'),
            beforeEnter: ifAuthenticated
        },
    ]
}
