import HttpErrorPage from '@/pages/errors/httpErrorPage'

export default [
    {
        path: '/500',
        name: '500 - Server error',
        component: HttpErrorPage,
        props: { errorCode: 500 }
    },
    {
        path: '/404',
        name: '404',
        component: HttpErrorPage,
        props: { errorCode: 404 }
    },
    {
        path: '*',
        name: 'Unknown page',
        component: HttpErrorPage,
        props: { errorCode: 404 }
    }
]