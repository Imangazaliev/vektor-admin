export enum Roles {
    Admin = 'admin',
    Editor = 'editor',
    Moderator = 'moderator',
    Corrector = 'corrector'
}
