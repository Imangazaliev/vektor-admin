export enum WordType {
    Noun = "noun",
    Verb = "verb",
    Particle = "particle"
}
