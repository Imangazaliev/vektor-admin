export default {
    Opened: "opened",
    Correction: "on_correction",
    Rejected: "rejected",
    Accepted: "accepted"
}
