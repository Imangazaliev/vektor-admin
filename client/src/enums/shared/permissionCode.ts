export  type PermissionCode =
    'admin-panel:view' |
    'admin-panel:settings' |
    'users:manage' |
    'words:status-changing' |
    'words:manage' |
    'words:delete' |
    'revisions:view' |
    'revisions:manage' |
    'events:view' |
    'statistic:view' |
    'books:view' |
    'books:manage' |
    'texts:view' |
    'texts:manage'
