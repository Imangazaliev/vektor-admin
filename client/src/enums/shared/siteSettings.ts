export class SiteSettings {
    publicUserId!: number
    isLibraryEnabled!: boolean
    isPublicEditingEnabled!: boolean
}
