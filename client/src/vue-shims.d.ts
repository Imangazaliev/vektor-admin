declare module 'plyr'  {
    export class  Plyr {
        setup: any
    }
}

export interface VueUploadItem {
    id: string;
    readonly fileObject?: boolean;
    name?: string;
    size?: number;
    type?: string;
    active?: boolean;
    error?: Error | string;
    success?: boolean;
    postAction?: string;
    putAction?: string;
    timeout?: number;
    data?: {
        [key: string]: any;
    };
    headers?: {
        [key: string]: any;
    };
    response?: {
        [key: string]: any;
    };
    progress?: string;
    speed?: 0;
    file?: Blob;
    xhr?: XMLHttpRequest;
    el?: HTMLInputElement;
    iframe?: HTMLElement;
    [key: string]: any;
}
