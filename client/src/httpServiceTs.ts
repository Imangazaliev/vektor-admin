import axios from 'axios'

axios.defaults.withCredentials = true

const baseUrl = process.env.NODE_ENV === 'development' ? "http://localhost:3000" : "https://kalimat.su";

const axiosService = axios.create({
    baseURL: `${baseUrl}/api`,
    withCredentials: true,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})

export default axiosService
