module.exports = {
    apps : [{
        name: "kalimat-website",
        script: "./build/start.js",
        cwd: "/var/www/kalimat-website/current",
        args: "",
        error_file : "./server-logs/errors.log",
        out_file : "./server-logs/out.log",
        log_file : "./server-logs/full.log",
        env: {
            NODE_ENV: "development",
        },
        env_production: {
            NODE_ENV: "production",
            SERVER_PORT: 3000,
            DB_USERNAME: "kalimat",
            DB_PASSWORD: "imangazaliev05",
            DB_NAME: "kalimatdb",
            DB_PORT: 5432,
            DB_HOST: "localhost",
            WEBSITE_URL: "https://kalimat.su"
        }
    }],
    deploy : {
        // "production" is the environment name
        production : {
            user : "root",
            host : ["5.101.50.21"],
            ref : "origin/master",
            repo : "git@gitlab.com:kalimat/kalimat-website.git",
            path : "/var/www/kalimat-website",
            //cwd: "/var/www/kalimat-website/current",
            //команда в скобках выполняется без смены текущей директории
            'post-deploy' : getPostDeployCommand()
        }
    }
}

function getPostDeployCommand() {
    const serverBuild = "npm install; npm run build-server-prod;"
    const clientBuild = "(cd client; npm install; npm run build-front);"
    const databaseMigrations = "npm run migration:up;"
    const serverRestart = "pm2 startOrRestart ecosystem.config.js --env production"
    return serverBuild + clientBuild + databaseMigrations + serverRestart
}
